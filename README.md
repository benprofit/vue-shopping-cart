# Small shopping cart app built in vue.

steps to run:

### clone:
```
git clone git clone https://benprofit@bitbucket.org/benprofit/vue-shopping-cart.git
```

### install dependencies
```
yarn install
```

### create firebase db w/ congfig keys and IDs

[Firebase tutorial](https://firebase.google.com/docs/database/web/start)

### create environmental variables in root dir
```
touch .env
```
then copy and paste keys in this format
```
VUE_APP_APIKEY=XXXXXXXXXXXXXXXXXX
VUE_APP_AUTHDOMAIN=XXXXXXXXXXXXXXXXX
VUE_APP_DB=XXXXXXXXXXXXXXXXXXX
VUE_APP_PID=XXXXXXXXXXXXXX
VUE_APP_SB=XXXXXXXXXXXXXXXX
VUE_APP_SID=XXXXXXXXXXXXXXX
VUE_APP_APPID=1:XXXXXXX:web:XXXXXXXXXXXX
VUE_APP_MID=G-XXXXXXXXXXXXXX
```

### run locally:
```
yarn serve
```
