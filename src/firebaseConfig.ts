import firebase from "firebase/app";
import "firebase/database";

const config = {
  apiKey: process.env.VUE_APP_APIKEY,
  authDomain: process.env.VUE_APP_AUTHDOMAIN,
  databaseURL: process.env.VUE_APP_DB,
  projectId: process.env.VUE_APP_PID,
  storageBucket: process.env.VUE_APP_SB,
  messagingSenderId: process.env.VUE_APP_SID,
  appId: process.env.VUE_APP_APPID,
  measurementId: process.env.VUE_APP_MID
};

firebase.initializeApp(config);
const databaseRef = firebase.database().ref();
export const cartRef = databaseRef.child("cart");

export default firebase;
